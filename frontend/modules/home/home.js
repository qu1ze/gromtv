angular.module('gromtv').controller('HomeCtrl',function($scope, socketio){
  
  var homeSocket = socketio.connect('/site');
  
  homeSocket.on('connect', function() {
    homeSocket.emit('test');
    homeSocket.on('event', function(data) {
      console.log('Client: site event fired!');
      console.log(data);
    });

    homeSocket.on('disconnect', function() {
      console.log('disconnected');
      homeSocket.removeAllListeners();
    });
  });     
  
  $scope.$on('$destroy', function (event) {
    console.log('$destroy HomeCtrl');
    homeSocket.removeAllListeners();    
  });
});