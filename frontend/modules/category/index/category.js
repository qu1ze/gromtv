angular.module('gromtv').controller('CategoryCtrl',function($scope, socketio, categoryContext, categoryModel){
  $scope.categories = categoryContext;

  $scope.delete = function($index) {
    categoryModel.delete($scope.categories[$index]._id).then(function(response){
      $scope.categories.splice($index, 1);
    });
  }
});