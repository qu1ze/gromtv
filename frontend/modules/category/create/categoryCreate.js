angular.module('gromtv').controller('CategoryCreateCtrl',function($scope, socketio, categoryModel, $state){
  $scope.save = function(category) {
    categoryModel.insert(category).then(function(data) {
      $state.go('category');
    });
  };
});