angular.module('gromtv').controller('CategoryEditCtrl',function($scope, socketio, categoryModel, categoryContext, $state){
  $scope.category = categoryContext;
  $scope.save = function(category) {
    categoryModel.update(category._id, category).then(function(data) {
      $state.go('category');
    });
  };
});