angular.module('gromtv').controller('UserEditCtrl',function($scope, $state, $stateParams, context, userContext, userModel){
  $scope.user = userContext;
  
  $scope.save = function(user) {   
    if ($scope.userEditForm.$valid) {      
      userModel.update(context.user._id, user)
        .then(function(data) {          
          $state.go('profile', { id : context.user._id });
        });        
    }
  }
});