angular.module('gromtv').controller('PostEditCtrl',function($scope, socketio, postContext, postModel, categoryModel, categoryContext, $sce, $state){
  $scope.categories = categoryContext;
  $scope.post = postContext;
  $scope.category = postContext.category;
  $scope.save = function(post) {
    var newPost = angular.copy(post);
    if(newPost && newPost.category) {
      newPost.category = newPost.category._id;
    }
    postModel.update(newPost._id, newPost).then(function(data) {
      $state.go('post');
    });
  }

  $scope.trustAsHtml = function(value) {
    return $sce.trustAsHtml(value);
  };

  $scope.refreshCategories = function(categoryName) {
    categories = [];
    var re = new RegExp(categoryName, 'i');
    categoryContext.forEach(function(category){
      if(re.test(category.name)) {
        categories.push(category);
      }
    });
    $scope.categories = categories;
  };
});