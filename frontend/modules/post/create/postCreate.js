angular.module('gromtv').controller('PostCreateCtrl',function($scope, socketio, postModel, categoryContext, $sce, $state){
  $scope.categories = categoryContext;

  $scope.save = function(post) {
    var newPost = angular.copy(post);
    if(newPost && newPost.category) {
      newPost.category = newPost.category._id;
    }
    postModel.insert(newPost).then(function(data) {
      $state.go('post');
    });
  }

  $scope.trustAsHtml = function(value) {
    return $sce.trustAsHtml(value);
  };

  $scope.refreshCategories = function(categoryName) {
    categories = [];
    var re = new RegExp(categoryName, 'i');
    categoryContext.forEach(function(category){
      if(re.test(category.name)) {
        categories.push(category);
      }
    });
    $scope.categories = categories;
  };
});