angular.module('gromtv').controller('PostCtrl',function($scope, socketio, postContext, postModel){
  $scope.posts = postContext;

  $scope.delete = function($index) {
    postModel.delete($scope.posts[$index]._id).then(function(response){
      $scope.posts.splice($index, 1);
    });
  }
});