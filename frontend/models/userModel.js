angular.module('gromtv').factory('userModel',function(model) {
  
  function baseUrl() { return '/api/user/'; }
  
	var user = {
    
    findByPk : function(id) {
      return model.get([baseUrl(), id].join(''));
    },
    
    findAll : function(){
      return model.get(baseUrl());
    },
    
    update : function(id, data) {
      return model.put([baseUrl(), id].join(''), data);
    },
    
    getRelations : function(id, type, config) {
      return model.get([baseUrl(), type, '/', id].join(''), config);
    }
  };

	return user;
});