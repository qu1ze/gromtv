angular.module('gromtv').factory('categoryModel',function(model) {
  
  function baseUrl() { return '/api/category/'; }

  return {

    findByPk : function(id) {
      return model.get([baseUrl(), id].join(''));
    },

    findAll : function(config){
      return model.get(baseUrl(), config);
    },

    update : function(id, data) {
      return model.put([baseUrl(), id].join(''), data);
    },

    insert : function(data) {
      return model.post(baseUrl(), data);
    },

    delete : function(id) {
      return model.delete([baseUrl(), id].join(''));
    }

  };

});