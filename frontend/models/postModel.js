angular.module('gromtv').factory('postModel',function(model) {
  
  function baseUrl() { return '/api/post/'; }

  return {
    
    findByPk : function(id) {
      return model.get([baseUrl(), id].join(''));
    },

    findByPkWithCategory : function(id) {
      return model.get([baseUrl(), id].join(''), {params: {category: 1}});
    },
    
    findAll : function(){
      return model.get(baseUrl());
    },

    findAllByCategoryId : function(id){
      return model.get([baseUrl(), 'category/', id].join(''));
    },
    
    update : function(id, data) {
      return model.put([baseUrl(), id].join(''), data);
    },

    insert : function(data) {
      return model.post(baseUrl(), data);
    },

    delete : function(id) {
      return model.delete([baseUrl(), id].join(''));
    }

  };
});