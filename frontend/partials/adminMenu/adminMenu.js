angular.module('gromtv').controller('AdminMenuCtrl',function($scope, $state, context){
  $scope.menuItems = [
    {title: 'Home', state: 'home', sref: 'home'},
    {title: 'Create Category', state: 'categoryCreate', sref: 'categoryCreate'},
    {title: 'List Categories', state: 'category', sref: 'category'},
    {title: 'Create Post', state: 'postCreate', sref: 'postCreate'},
    {title: 'List Posts', state: 'post', sref: 'post'},
  ];

  $scope.activeState = $state.current.name;
});