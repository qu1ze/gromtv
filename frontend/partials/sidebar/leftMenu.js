angular.module('gromtv').controller('LeftMenuCtrl',function($scope, $state, context){
  $scope.menuItems = [
    {title: 'Home', state: 'home', sref: 'home'},
    {title: 'Profile', state: 'profile', sref: 'profile({ id : "'+context.user._id+'" })'},
    {title: 'Edit Profile', state: 'user.edit', sref: 'user.edit'}
  ];

  $scope.activeState = $state.current.name;
});