angular.module('gromtv').factory('socketio',function($rootScope) {
  
  function _errorHandler(message) {    
    console.error(message);      
  }
  
	var _io = io
    , socketio = {
      
      _socket : null,
      _callbacks : [],      
      
      connect : function(url) {     
        this._socket = _io(url);
        return this;
      },
      
      emit : function(event, data, callback) {        
        var self = this;
        if (!self._socket) {
          return _errorHandler('No socket connected!');
        }
         
        self._socket.emit(event, data, function () {          
          var args = arguments;
          $rootScope.safeApply(function () {
            if (callback) {
              callback.apply(self._socket, args);
            }
          });
        })
      },
      
      on : function(event, callback) {
        var self = this;
        if (!self._socket) {
          return _errorHandler('No socket connected!');
        }              
        
        function _onCallback() {
          var args = arguments;                        
          $rootScope.safeApply(function () {
            callback.apply(self._socket, args);
          });
        }
        
        self._callbacks.push({ event : event, callback : _onCallback });
        self._socket.on(event, _onCallback);
      },          
        
      getSocket : function() {
        return this._socket;
      },
      
      removeAllListeners : function() {
        var self = this;
        self._callbacks.forEach(function(item) {
          if (!~['connect'].indexOf(item.event)) {
            console.log('Client: removing listener on event - ' + item.event);
            self._socket.removeListener(item.event, item.callback);
          }
        });
        
        self._callbacks = [];
      }
    };

	return socketio;
});