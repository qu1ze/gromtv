angular.module('gromtv').factory('context',function($rootScope, userModel) {

	var context = {
    
    init: function(id) {
      if (id) {        
        var self = this;
        
        self.user = { _id : id };
        /*
        Updating context...
        userModel.findByPk(id)
          .then(function(data) {            
            self.user = data;
            
            console.log('updated context', context);
            $rootScope.$broadcast('context:updated');
          }, function() {
            console.error(data);            
          });                        
        */
      }
    }
    
  };

	return context;
});