angular.module('gromtv').factory('model',function($http, $q, $state, _) {

  function _handleErrors(data, status) {
    console.log("Server error: " + data + 'Status: ' + status);

    switch (status) {          
      case 401:
        $state.go('home');
        break;      
      default:          
        $state.go('error', { status : status });
        break;
    }        
  }
  
  function _request(config) {
    var deferred = $q.defer();        
    
    $http(config)
      .success(function(data, status, headers, config, statusText) {             
        deferred.resolve(data, status, headers, config, statusText);        
      })
      .error(function(data, status, headers, config, statusText) {
        _handleErrors(data, status);
        deferred.reject(data, status, headers, config, statusText);
      });
    
    return deferred.promise;
  }
    
	var model = {
      
    get : function (url, config) {
      return _request(_.extend({
        url : url,
        method : 'GET'
      }, config || {}));     
    },
    
    post : function(url, data, config) {
      return _request(_.extend({
        url : url,
        method : 'POST',
        data : data
      }, config || {}));  
    },
    
    put : function(url, data, config) {
      return _request(_.extend({
        url : url,
        method : 'PUT',
        data : data
      }, config || {}));  
    },
    
    delete : function(url, data, config) {
      return _request(_.extend({
        url : url,
        method : 'DELETE'        
      }, config || {}));  
    },
        
  };

	return model;
});