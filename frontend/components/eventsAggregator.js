angular.module('gromtv').factory('eventsAggregator',function($rootScope) {

	var eventsAggregator = {
    subscriptions: [],

    emit: function (name, data) {
      $rootScope.$emit(name, data);
    },
    
    on: function (name, callback) {            
      var self = this
        , _callback = function(){
            var args = arguments;  
            $rootScope.safeApply(function () {
              callback.apply(null, args);
            });
          };
          
      self.subscriptions.push({
        event : name,
        callback : $rootScope.$on(name, _callback),
        originCallback : callback // to recognize callback while removing
      });              
    },
    
    /*
      Params:
        name - event name
        callback - event callback
        isAll - flag to remove all similar handlers (default: true)
    */
    off: function (name, callback, isAll) {      
      var self = this
        , isFound = this.subscriptions.some(function(item, index) {
            if (callback) {
              if (item.originCallback == callback && item.event == name) {
                return self.removeSubscription(item, index);            
              }
            } else if (item.event == name) {
              return self.removeSubscription(item, index);                     
            }
          });      
                
      // off all similar handlers
      // highly recommended to use
      if (isAll === undefined || isAll) {
        isFound && this.off(name, callback, isAll);             
      }                
    },
    
    removeSubscription : function(subscription, index) {
      subscription.callback(); // deregister handler
      this.subscriptions.splice(index, 1);
      return true;
    }
  };

	return eventsAggregator;
});