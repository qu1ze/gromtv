if (typeof console === "undefined" || typeof console.log === "undefined") {
  console = {};
  console.log = function() {};
}

angular.module('gromtv', ['ui.bootstrap','ui.utils','ui.router','ngAnimate','ui.select','textAngular']);

angular.module('gromtv').value('$', $);

angular.module('gromtv').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  'use strict';

  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('home', {
    url: '/',
    templateUrl: '/modules/backend/backend.html',
    controller: "BackendCtrl",
  });

  $stateProvider.state('error', {
    url : '/error?status',
    templateUrl: '/modules/error/error.html',
    controller: 'ErrorCtrl'
  });

  $stateProvider.state('category', {
    url : '/category',
    templateUrl: '/modules/category/index/category.html',
    controller: 'CategoryCtrl',
    resolve: {
      categoryContext : function(categoryModel) {
        return categoryModel.findAll();
      }
    }
  });

  $stateProvider.state('categoryCreate', {
    url : '/category/create',
    templateUrl: '/modules/category/create/categoryCreate.html',
    controller: 'CategoryCreateCtrl'
  });

  $stateProvider.state('categoryEdit', {
    url : '/category/edit/:id',
    templateUrl: '/modules/category/edit/categoryEdit.html',
    controller: 'CategoryEditCtrl',
    resolve: {
      categoryContext : function($stateParams, categoryModel) {
        return categoryModel.findByPk($stateParams.id);
      }
    }
  });

  $stateProvider.state('post', {
    url : '/post',
    templateUrl: '/modules/post/index/post.html',
    controller: 'PostCtrl',
    resolve: {
      postContext : function(postModel) {
        return postModel.findAll();
      }
    }
  });

  $stateProvider.state('postEdit', {
    url : '/post/edit/:id',
    templateUrl: '/modules/post/edit/postEdit.html',
    controller: 'PostEditCtrl',
    resolve: {
      postContext : function($stateParams, postModel) {
        return postModel.findByPkWithCategory($stateParams.id);
      },
      categoryContext : function(categoryModel) {
        return categoryModel.findAll();
      }
    }
  });

  $stateProvider.state('postCreate', {
    url : '/post/create',
    templateUrl: '/modules/post/create/postCreate.html',
    controller: 'PostCreateCtrl',
    resolve: {
      categoryContext : function(categoryModel) {
        return categoryModel.findAll();
      }
    }
  });

});

angular.module('gromtv').controller('AppCtrl', function($scope, context){
  $scope.init = function(id){
    context.init(id);
  };

  $scope.headerHtml = '/partials/header/backendHeader.html';
  $scope.footerHtml = '/partials/footer/footer.html';
  $scope.menuHtml = '/partials/adminMenu/adminMenu.html';
});

angular.module('gromtv').run(function($rootScope) {

  $rootScope.safeApply = function(fn) {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

});

angular.module('gromtv').factory('_', function() {
  return window._;
});