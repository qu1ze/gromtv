if (typeof console === "undefined" || typeof console.log === "undefined") {
  console = {};
  console.log = function() {};
}

angular.module('gromtv', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('gromtv').value('$', $);

angular.module('gromtv').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  'use strict';

  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('home', {
    url: '/',
    templateUrl: '/modules/home/home.html',
    controller: "HomeCtrl",
  });

  $stateProvider.state('user', {
    url: '/user',
    abstract: true,
    template: '<ui-view/>',
    resolve: {
      userContext : function(context, userModel) {
        console.log(userModel);
        console.log(context);
        return userModel.findByPk(context.user._id);
      }
    }
  });

  $stateProvider.state('user.edit', {
    url: '/edit',
    templateUrl: '/modules/user/edit/userEdit.html',
    controller: 'UserEditCtrl'
  });

  $stateProvider.state('profile', {
    url: '/profile/:id',
    templateUrl: '/modules/user/profile/userProfile.html',
    controller: 'UserProfileCtrl',
    resolve: {
      userProfile : function($stateParams, userModel) {
        return userModel.findByPk($stateParams.id);
      },
      me : function(userModel, context) {
        return userModel.findByPk(context.user._id);
      },
    }
  });

  $stateProvider.state('error', {
    url : '/error?status',
    templateUrl: '/modules/error/error.html',
    controller: 'ErrorCtrl'
  });

});

angular.module('gromtv').controller('AppCtrl', function($scope, context){
  $scope.init = function(id){
    context.init(id);
  };

  $scope.headerHtml = '/partials/header/header.html';
  $scope.footerHtml = '/partials/footer/footer.html';
  $scope.sidebar = '/partials/sidebar/leftMenu.html';
});

angular.module('gromtv').run(function($rootScope) {

  $rootScope.safeApply = function(fn) {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

});

angular.module('gromtv').factory('_', function() {
  return window._;
});