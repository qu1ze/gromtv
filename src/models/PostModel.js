var util = require('util')
  , underscore = require('underscore')
  , superClass = require('../components/ActiveRecord.js');

function PostModel(scenario) {
  superClass.call(this, scenario);
  
  this.setAttributes({
    title : null,
    mediaUrl : null,
    content : null,
    category : {} //relation
  });
}

util.inherits(PostModel, superClass);

PostModel.prototype.rules = function() {
  return [
    {
      rule : 'required',
      attributes: ['title', 'content', 'category']
    }
  ];
}

PostModel.prototype.tableName = function() {
  return 'post';
}

PostModel.model = function() {
  return PostModel.super_.model(PostModel);
}

PostModel.isYoutubeLink = function (link) {
  var parts = (new RegExp('(https?:\\/\\/)?(www[.])?(.+?)\\..+')).exec(link);
  return parts && ('3' in parts) && link && ~['youtube', 'youtu'].indexOf(parts[3]);
}

PostModel.getYoutubeVideoId = function (link) {
  var parts = (new RegExp('(.*v=)?([^=]*)[#?&]+')).exec(link.split('/')[3]+'&');
  return parts[parts.length - 1];
}

module.exports = PostModel;