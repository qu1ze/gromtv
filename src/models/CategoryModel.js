var util = require('util')
  , underscore = require('underscore')
  , superClass = require('../components/ActiveRecord.js');

function CategoryModel(scenario) {
  superClass.call(this, scenario);
  
  this.setAttributes({
    name : null,
  });
}

util.inherits(CategoryModel, superClass);

CategoryModel.prototype.rules = function() {
  return [
    {
      rule : 'required',
      attributes: ['name']
    }
  ];
}

CategoryModel.prototype.tableName = function() {
  return 'category';
}

CategoryModel.model = function() {
  return CategoryModel.super_.model(CategoryModel);
}

module.exports = CategoryModel;