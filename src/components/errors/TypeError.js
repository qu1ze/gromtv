var util = require('util');

function TypeError(instanceClassName, allowedInstanceName) {	
	if (!instanceClassName || !allowedInstanceName)
		throw Error('Expected class name or given class name is undefined');
	
	if (instanceClassName === allowedInstanceName)
		throw Error('Expected and given classes are the same');
	
    this.message = 'Expected ' + allowedInstanceName + ', but ' + instanceClassName + ' was given!';
	Error.captureStackTrace(this, TypeError);
}

util.inherits(TypeError, Error);

TypeError.prototype.name = 'TypeError';

module.exports = TypeError;