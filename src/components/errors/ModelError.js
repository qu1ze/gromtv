var util = require('util');

function ModelError(attribute, message) {
	if (!attribute || !message)
		throw Error('Empty ModelError params!');
		
	this.attribute = attribute;
	this.message = message;
	Error.captureStackTrace(this, ModelError);
}

util.inherits(ModelError, Error);

ModelError.prototype.name = 'ModelError';


module.exports = ModelError;