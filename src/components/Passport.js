var LocalStrategy = require('passport-local').Strategy
  , FacebookStrategy = require('passport-facebook').Strategy
  , TwitterStrategy = require('passport-twitter').Strategy
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
  , underscore = require('underscore')
  , mongodb = require('mongodb')
  , nconf = require('./../config').nconf
  , UserModel = require('./../models/UserModel');

module.exports = function( passport ) {

  function findOrCreate(data, done, callback) {
    UserModel.model().findByAttributes(data, function(err, user) {
      if (err) { return done(err); }

      if (user) {
        return done(null, user);
      }

      callback && callback();
    });
  }

  passport.serializeUser(function(user, done) {
    done(null, user.getAttributes('_id') || user.getAttributes());
  });

  passport.deserializeUser(function(id, done) {
    if (typeof id == 'string') {
      UserModel.model().findByPk(id, function(err, user) {
        if (err) { return done(err) };

        done(null, user);
      });
    } else {
      // for Twitter only. id = raw UserModel object attributes
      done(null, id);
    }
  });

  passport.use(new LocalStrategy({
      usernameField : 'email',
      passwordField : 'password'
    },
    function(email, password, done) {
      UserModel.model().findByAttributes({ email : email }, function(err, user) {
        if (err) { return done(err); }

        if (!user) {
          return done(null, false, { message : 'Incorrect email'});
        }
        if (!user.validPassword(password)) {
          return done(null, false, { message : 'Incorrect password'});
        }
        done(null, user);
      });
    }
  ));

  passport.use(new FacebookStrategy({
      clientID: '1457455724499365',
      clientSecret: 'a0797f4199a4592d0691f808f2590564',
      callbackURL: 'http://gromtv.net/auth/facebook/callback',
      profileFields: ['id', 'displayName', 'photos', 'name', 'gender', 'emails']
    },
    function(accessToken, refreshToken, profile, done) {
      var profileData = profile._json;

      findOrCreate({ email : profileData.email }, done, function() {
        var user = new UserModel;
        user.setAttributes({
          firstname : profileData.first_name,
          lastname : profileData.last_name,
          gender : profileData.gender,
          email : profileData.email
        });
        user.save(function(err, result) {
          if (err) { return done(err); }

          findOrCreate({ email : profileData.email }, done);
        });
      });

    }
  ));

  passport.use(new TwitterStrategy({
      consumerKey: 'gn6bTBdkmvv9HPXYi4mPA1lQ0',
      consumerSecret: 'CvvAiybobScLCl229NVYz50BOz4LrimTrTifuCl8xtiFUXeJfU',
      callbackURL: 'http://gromtv.net/auth/twitter/callback',
    },
    function(token, tokenSecret, profile, done) {
      findOrCreate({ login : profile.username }, done, function() {
        var user = new UserModel
          , names = profile.displayName.split(' ');

        user.setAttributes({
          firstname : names && names[0] ? names[0] : null,
          lastname : names && names[1] ? names[1] : null,
          login : profile.username
        });

        done(null, user);
      });

    }
  ));

  passport.use(new GoogleStrategy({
      clientID: "143049282351-1gh3u01b65i0hj69ed7tbrk0sts8hh6j",
      clientSecret: "Ruq0cvJ8ngylEKgt-EHm9qQI",
      callbackURL: 'http://gromtv.net/auth/google/callback'
    },
    function(accessToken, refreshToken, profile, done) {
console.log(profile);
      if (!profile.emails || !profile.emails.length) {
        done('No emails provided');
      }

      findOrCreate({ email : profile.emails[0].value }, done, function() {
        var user = new UserModel;
        user.setAttributes({
          firstname : profile.name ? profile.name.givenName : null,
          lastname : profile.name ? profile.name.familyName : null,
          email : profile.emails[0].value
        });
        user.save(function(err, result) {
          if (err) { return done(err); }

          findOrCreate({ email : profile.emails[0].value }, done);
        });
      });
    }
  ));

};
