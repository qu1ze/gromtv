var eventEmitter = require('events').EventEmitter
  , util = require('util')
  , ModelError = require('./errors/ModelError.js')
  , mongo = require('mongodb')
  , async = require('async')
  , Validator = require('./Validator.js')
  , underscore = require('underscore');

function Model(scenario) {	
	
	var self = this
	  , _connection = typeof mongoDb !== 'undefined' ? mongoDb : null;
	
	this.getConnection = function() {		
		return _connection;
	}
		
	this._attributes = {};	
	this._errors = {};	
	this._validators = [];
	this.setScenario(scenario);
	this.afterConstruct();
}

util.inherits(Model, eventEmitter);

Model.prototype.setScenario = function(scenario) {
	this._scenario = scenario ? scenario : 'insert';
}

Model.prototype.getScenario = function() {
	return this._scenario;
}

Model.prototype.rules = function() {
	return [];
}

Model.prototype.afterConstruct = function() {
	
}

Model.prototype.afterValidate = function(callback) {
	callback(true);
}

Model.prototype.beforeValidate = function beforeValidate(callback) {	
	callback(true);
}

Model.prototype.getValidators = function() {
	
	var scenario = this.getScenario()
	  , rules = this.rules()	  
	  , validators = [];
	  
	if (underscore.isEmpty(this._validators))
		this._validators = Validator.createValidators(rules, this);

	underscore.each(this._validators, function(validator, index) {		
		if (validator.applyTo(scenario)) {			
			validators.push(validator);
		}
	});
	
	return validators;
}

Model.prototype.validate = function(callback) {
	
	var self = this;	
	if (!underscore.isEmpty(self._errors))
		self.pullErrors();
		
	self.beforeValidate(function(beforeValidationResult) {
		if (beforeValidationResult) {			
			async.eachSeries(self.getValidators(), function(validator, next) {
				validator.validate(self, next);
			}, function(err) {
				if (err) {
					callback( false );
					return false;
				}				
				self.afterValidate(function(afterValidationResult) {
					callback( afterValidationResult && !self.hasErrors() );	
				});	
			});													
		} else	    
			callback( false );
	});	
}

/**
 * Returns a value indicating whether there is any validation error.
 * @param string attribute attribute name. Do not use to check all attributes.
 * @return boolean whether there is any error.
 */
Model.prototype.hasErrors = function(attribute) {	
    return !attribute ? !underscore.isEmpty(this._errors) : !!this._errors[attribute];
}

/**
 * Gets array of errors for specific attribute by it name
 * @param {string} attribute The name of attribute
 */
Model.prototype.getError = function(attribute) {
    return this._errors[attribute] ? this._errors[attribute] : null;
}

/**
 * Returns the errors for all attribute or a single attribute.
 * @param string attribute attribute name
 * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
 */
Model.prototype.getErrors = function(attribute) {
    if (!attribute) 
		return this._errors;
	else
		return this._errors[attribute] ? this._errors[attribute] : [];
}

/**
 * Returns and removes errors for all attributes or a single attribute.
 * @param string attribute attribute name.
 */
Model.prototype.pullErrors = function(attribute) {
    var errs = attribute && this._errors[attribute] ? this._errors[attribute] : this._errors;
    if (!attribute) 
		this._errors = {};
	else
		delete this._errors[attribute]		
    return errs;
}

/**
 * Adds error(-s) to model errors by attribute name.
 * @param {string} attributeName The name of attribute
 * @param {Error|array} message The message of error or an array of error messages
 */
Model.prototype.addError = function(attribute, message) {	
    var errors = message instanceof Array ? message : [message];
    this._errors[attribute] = this._errors[attribute] ? this._errors[attribute].concat(errors) : errors;
}

/**
 * Adds errors to model errors. 
 * @param {Error|array} message The array of error messages objects
 */
Model.prototype.addErrors = function(errors) {
	var self = this;
	underscore.each(errors, function(error) {				
		self.addError(error.attribute, error.messages);
	});
}

/**
 * Sets the attribute values.
 * @param attributes to be set. 
 */
Model.prototype.setAttributes = function(attributes) {		
	for (var fieldName in attributes)
		this._attributes[fieldName] = attributes[fieldName];
}

/**
 * Sets the attributes to be null.
 * @param array attributes list of attributes to be set null. If this parameter is not given,
 * all attributes will have their values unset. 
 */
Model.prototype.unsetAttributes = function(attributes) {
	var self = this;
	if (!attributes)
		self._attributes = {};
	else
		underscore.each(attributes, function(attribute) {
			self._attributes[attribute] = null;
		});
}

/**
 * Returns all attributes.
 * @param array names list of attributes whose value needs to be returned.
 * Defaults to null, meaning all attributes as listed in will be returned.
 * If it is an array, only the attributes in the array will be returned.
 * @return array attributes.
 */
Model.prototype.getAttributes = function(attr) {
	if (!attr) {
		return this._attributes;
	} else if (typeof attr == 'string') {
		return attr in this._attributes  ? this._attributes[attr] : null;
	} else {
		var results = {}
		  , self = this;		
		underscore.each(attr, function(value) {			
			if (value in self._attributes)
				results[value] = self._attributes[value];
		});		
		return results;
	}
}

module.exports = Model;