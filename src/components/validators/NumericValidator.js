var util = require('util')
    , superClass = require('../Validator.js');

/**
 * @example rule - required: {attributes: [''], message: 'My message', on: ['insert'], except: ['update']},
 */
function NumericValidator(ruleParams) {
    superClass.call(this, ruleParams);
}

util.inherits(NumericValidator, superClass);

NumericValidator.prototype.validateAttribute = function(model, attribute, next) {
    if ((model.getAttributes(attribute) instanceof Array) || isNaN(parseFloat(model.getAttributes(attribute))))
		this.addError(model, attribute);
		
	next();
}

NumericValidator.prototype.getMessage = function(attribute) {
    return attribute + ' must be a numeric value!';
}

module.exports = NumericValidator;