var util = require('util')
    , superClass = require('../Validator.js');

/**
 * @example rule - required: {attributes: [''], message: 'My message', on: ['insert'], except: ['update']},
 */
function IntegerValidator(ruleParams) {
    superClass.call(this, ruleParams);
}

util.inherits(IntegerValidator, superClass);

IntegerValidator.prototype.validateAttribute = function(model, attribute, next) {	
    if (model.getAttributes(attribute) % 1 !== 0 || parseInt(model.getAttributes(attribute)) % 1 !== 0)
		this.addError(model, attribute);
		
	next();
}

IntegerValidator.prototype.getMessage = function(attribute) {
    return attribute + ' must be an integer value!';
}

module.exports = IntegerValidator;