var express = require('express')
  , path = require('path')      
  , superCtrl = require('./Controller.js')  
  , fs = require('fs');

function _parse(initPath, callback) {

  fs.readdirSync(initPath).forEach(function(name) {
	
    var itemPath = path.join(initPath, name)
      , stat = fs.statSync(itemPath);

    if (stat && stat.isDirectory(itemPath)) {
              
      //recursive dir reading
      _parse(itemPath, callback);

    } else {         
      callback(itemPath, name);
    }
    
	});		
}  

module.exports = {
  controllers : function(application) {	
    _parse(path.join(__dirname, '..', 'controllers'), function(itemPath, name) {
    
      var controller = require(itemPath)        
        , controllerName = controller.name || name
        , app = express()
        , viewFolderName = controllerName.toLowerCase().substring(0, controllerName.indexOf('Controller'));					
               
      // connecting views
      app.set('views', path.join(__dirname, '..', 'views', viewFolderName));
      
      // connection routers
      app.use(controller.prefix || superCtrl.prefix, controller.instance);
      
      application.use(app);
      
    });	
  },
  
  sockets : function(io) {       
    _parse(path.join(__dirname, '..', 'sockets'), function(itemPath, name) {        
      require(itemPath)(io);                              
    });          
  }
}