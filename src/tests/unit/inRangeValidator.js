var Model = require('../../components/Model.js');

module.exports = {

	setUp : function(callback) {		
		this.model = new Model('insert');
		this.model.setAttributes({
			year : 1993
		});
		this.model.rules = function() {
			return [
				{
					rule : 'inrange',
					attributes : ['year'], 
					range : [1993, 1992, 1994]
				}
			];
		}		
		callback();
	},
	tearDown : function(callback) {
		this.model = {};
		callback();
	},
	successValidations : function(test) {		
		test.expect(1);						
		this.model.validate(function(result) {
			test.ok(result, 'Validation passed (values is in range)');					
			test.done();
		});							
	},
    failedValidations : function(test) {       
		test.expect(1);							
		this.model.setAttributes({
			year : 2000
		});		
		this.model.validate(function(result) {
			test.equal(result, false, 'Failed validation passed (value is not in range)');		
			test.done();
		});		        
    }	
};