var underscore = require('underscore');
  
module.exports = function(io) {
  var site = io
    .of('/site')
    .on('connection', function(socket) {
      console.log('on site connection'); 
      console.log(io.sockets.sockets.length);
      
      socket.on('test', function(data) {
        console.log('Site Socket Test callback');
        
        socket.emit('event', { path : 'site' });
      });
      
      socket.on('disconnect', function(socket) {
        console.log('Site Socket: disconnected');
      });
    });
};