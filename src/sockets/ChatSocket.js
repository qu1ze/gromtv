var underscore = require('underscore');
  
module.exports = function(io) {
  var chat = io
    .of('/chat')
    .on('connection', function(socket) {
      console.log('on chat connection');   
      console.log(io.sockets.sockets.length);
      
      socket.on('test', function() {
        console.log('Chat Socket Test callback');
        
        chat.emit('event', { path : 'chat' }); 
      });
      
      socket.on('disconnect', function(socket) {
        console.log('Chat Socket: disconnected');
      });
    });
};