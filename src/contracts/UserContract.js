var Contract = require('./../components/Contract')
  , UserModel = require('./../models/UserModel')
  , underscore = require('underscore');

module.exports = underscore.extend({
  validate : function(req, callback) {
    req.checkBody('email', 'Email is required').notEmpty().isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('passwordConfirmation', 'Confirmation password is required').notEmpty().equals(req.body.password);
    
    callback( Contract.errorResponse(req.validationErrors()), underscore.omit(req.body, 'passwordConfirmation') );
  },
  
  validateTwitter : function(req, callback) {
    req.checkBody('email', 'Email is required').notEmpty().isEmail();
    
    UserModel.model().findAllByAttributes({ email : req.body.email }, function(err, items) {
      if (err || items.length) {        
        return callback({
          email : [ 'This email is already in the system' ]
        });
      }
      
      callback( Contract.errorResponse(req.validationErrors()) );
    });    
  }
  
}, Contract);