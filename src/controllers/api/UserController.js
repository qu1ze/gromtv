var underscore = require('underscore')    
  , express = require('express')  
  , path = require('path')  
  , mongodb = require('mongodb')  
  , passport = require('passport')  
  , UserModel = require('./../../models/UserModel')
  , nconf = require('./../../config').nconf
  , suCtrl = require('./../../components/Controller.js')
  , router = express.Router();  

router.get('/api/user', function(req, res) {
  UserModel.model().findAll(function(err, users) {
    res.send(users._data);
  });
});

router.route('/api/user/:id')
  .get(function(req, res) {
    UserModel.model().findByPk(req.params.id, function(err, user) {
      res.send(underscore.omit(user.getAttributes(), 'salt', 'password', 'type', 'rating', 'coins'));
    });
  })
  .put(function(req, res) {
    UserModel.model().findByPk(req.params.id, function(err, user) {
      if (err || !user) {
        return res.send(500, err);
      }
      
      user.setAttributes(underscore.omit(req.body, '_id'));
      user.save(function(err, result) {
        if (!err) {
          res.send({ _id : req.params.id });
        } else {
          res.send(500);
        }
      });
    });
  });
  
module.exports = {  
  instance : router
};