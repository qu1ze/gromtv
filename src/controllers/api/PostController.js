var underscore = require('underscore')    
  , express = require('express')  
  , path = require('path')  
  , mongodb = require('mongodb')
  , PostModel = require('./../../models/PostModel')
  , CategoryModel = require('./../../models/CategoryModel')
  , nconf = require('./../../config').nconf
  , suCtrl = require('./../../components/Controller.js')
  , router = express.Router();  

router.route('/api/post')
  .get(function(req, res) {
    PostModel.model().findAll(function(err, posts) {
      res.send(posts._data);
    });
  }).post(function(req, res) {
    var post = new PostModel();
    post.setAttributes(underscore.omit(req.body, '_id'));
    post.save(function(err, result) {
      if (!err) {
        res.send(post.getAttributes());
      } else {
        res.send(500);
      }
    });
  });

router.route('/api/post/category/:categoryId')
  .get(function(req, res) {
    PostModel.model().findAllByAttributes({category: req.params.categoryId}, function(err, posts) {
      res.send(posts._data);
    });
  });

router.route('/api/post/:id')
  .get(function(req, res) {
    PostModel.model().findByPk(req.params.id, function(err, post) {
      console.log(post.getAttributes('category'));
      if(req.query.category) {
        CategoryModel.model().findByPk(post.getAttributes('category'), function(err, category) {
          post.setAttributes({category: category.getAttributes()});
          res.send(post.getAttributes());
        });
      } else {
        res.send(post.getAttributes());
      }
    });
  })
  .put(function(req, res) {
    PostModel.model().findByPk(req.params.id, function(err, post) {
      if (err || !post) {
        return res.send(500, err);
      }

      post.setAttributes(underscore.omit(req.body, '_id'));
      post.save(function(err, result) {
        if (!err) {
          res.send({ _id : req.params.id });
        } else {
          res.send(500);
        }
      });
    });
  }).delete(function(req, res) {
    PostModel.model().deleteByPk(req.params.id, function(err) {
      res.send(req.params.id);
    });
  });
  
module.exports = {  
  instance : router
};