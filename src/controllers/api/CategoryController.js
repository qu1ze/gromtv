var underscore = require('underscore')    
  , express = require('express')  
  , path = require('path')  
  , mongodb = require('mongodb')
  , CategoryModel = require('./../../models/CategoryModel')
  , PostModel = require('./../../models/PostModel')
  , nconf = require('./../../config').nconf
  , suCtrl = require('./../../components/Controller.js')
  , router = express.Router();  

router.route('/api/category')
  .get(function(req, res) {
    if(!req.query.name) {
      CategoryModel.model().findAll(function(err, categories) {
        res.send(categories._data);
      });
    } else {
      CategoryModel.model().findAllByAttributes({name: new RegExp(req.query.name, 'ig')}, function(err, categories) {
        res.send(categories._data ? categories._data : []);
      });
    }
  }).post(function(req, res) {
    var category = new CategoryModel();
    category.setAttributes(underscore.omit(req.body, '_id'));
    category.save(function(err, result) {
      if (!err) {
        res.send(category.getAttributes());
      } else {
        res.send(500);
      }
    });
  });

router.route('/api/category/:id')
  .get(function(req, res) {
    CategoryModel.model().findByPk(req.params.id, function(err, category) {
      res.send(category.getAttributes());
    });
  })
  .put(function(req, res) {
    CategoryModel.model().findByPk(req.params.id, function(err, category) {
      if (err || !category) {
        return res.send(500, err);
      }

      category.setAttributes(underscore.omit(req.body, '_id'));
      category.save(function(err, result) {
        if (!err) {
          res.send({ _id : req.params.id });
        } else {
          res.send(500);
        }
      });
    });
  }).delete(function(req, res) {
    PostModel.model().findAllByAttributes({category: req.params.id}, function(err, posts) {
      if(posts.length) {
        res.send(500);
      } else {
        CategoryModel.model().deleteByPk(req.params.id, function(err) {
          res.send(req.params.id);
        });
      }
    });
  });
  
module.exports = {  
  instance : router
};