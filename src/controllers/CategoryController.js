var underscore = require('underscore')
  , express = require('express')
  , path = require('path')
  , nconf = require('./../config').nconf
  , PostModel = require('./../models/PostModel')
  , CategoryModel = require('./../models/CategoryModel')
  , suCtrl = require('./../components/Controller.js');

var router = express.Router()
  , env = nconf.get('NODE_ENV')
  , prefix = '/category';

router.route(prefix+'/:id')
  .get(function(req, res) {
    CategoryModel.model().findAll(function(err, categories) {
      PostModel.model().findAllByAttributes({category: req.params.id}, function(err, posts) {
        var resultPosts = [];
        (posts._data ? posts._data.reverse() : []).forEach(function(post){

          if(PostModel.isYoutubeLink(post.mediaUrl)) {
            post.mediaUrl = 'https://i1.ytimg.com/vi/'+PostModel.getYoutubeVideoId(post.mediaUrl)+'/default.jpg';
          }
          post.content = post.content.replace((new RegExp('{youtube}.*?{\/youtube}', 'ig')), '');
          resultPosts.push(post);
        });
        CategoryModel.model().findByPk(req.params.id, function(err, category) {
          res.render('index', {
            title : category.getAttributes('name')+'. Гром TV - Громадське телебачення',
            posts : resultPosts,
            categories : categories._data ? categories._data : []
          });
        });
      });
    });
  });

module.exports = {
  instance : router
};
