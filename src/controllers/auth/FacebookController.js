var underscore = require('underscore')    
  , express = require('express')  
  , path = require('path')  
  , passport = require('passport')  
  , nconf = require('./../../config').nconf
  , UserModel = require('./../../models/UserModel')
  , UserContract = require('./../../contracts/UserContract')  
  , suCtrl = require('./../../components/Controller.js')
  , router = express.Router();

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
router.get('/auth/facebook',
  passport.authenticate('facebook', {
    scope: ['read_stream', 'email']
  })
);

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
router.get('/auth/facebook/callback', 
  passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/signin' 
  })
);
                                      
module.exports = {  
  instance : router
};