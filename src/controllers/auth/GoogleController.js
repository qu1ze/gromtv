var underscore = require('underscore')    
  , express = require('express')  
  , path = require('path')  
  , passport = require('passport')  
  , nconf = require('./../../config').nconf
  , UserModel = require('./../../models/UserModel')
  , UserContract = require('./../../contracts/UserContract')  
  , suCtrl = require('./../../components/Controller.js')
  , router = express.Router();

// Redirect the user to Google for authentication.  When complete, Google
// will redirect the user back to the application at
//     /auth/google/return
router.get('/auth/google', passport.authenticate('google', { scope: 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile' }));

// Google will redirect the user to this URL after authentication.  Finish
// the process by verifying the assertion.  If valid, the user will be
// logged in.  Otherwise, authentication has failed.
router.get('/auth/google/callback', 
  passport.authenticate('google', {
    successRedirect: '/',
    failureRedirect: '/signin' 
  })
);
                                      
module.exports = {  
  instance : router
};
