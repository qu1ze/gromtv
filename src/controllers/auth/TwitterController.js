var underscore = require('underscore')    
  , express = require('express')  
  , path = require('path')  
  , passport = require('passport')  
  , nconf = require('./../../config').nconf
  , UserModel = require('./../../models/UserModel')
  , UserContract = require('./../../contracts/UserContract')  
  , suCtrl = require('./../../components/Controller.js')
  , router = express.Router();

// Redirect the user to Twitter for authentication.  When complete, Twitter
// will redirect the user back to the application at
//   /auth/twitter/callback
router.get('/auth/twitter',
  passport.authenticate('twitter')
);

// Twitter will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
router.get('/auth/twitter/callback', passport.authenticate('twitter', {
    successRedirect: '/auth/twitter/authorize',
    failureRedirect: '/signin' 
}));

router.route('/auth/twitter/authorize')
  .get(function(req, res) { 
    if (req.user instanceof UserModel) {
      res.redirect('/');
    } else {
      res.render('authorize', {
        user : req.user,
        errors : false
      });
    }
  })
  .post(function(req, res) {
    UserContract.validateTwitter(req, function(errors) {
      if (!errors) {
        var user = new UserModel;
        
        user.setAttributes(req.body);
        user.save(function(err, result) {
          if (err) { return console.log(err); }
          
          req.login(user, function(err) {
            if (err) { return console.log(err); }
            res.redirect('/');
          });
        });
      } else {        
        res.render('authorize', {
          user : req.user,
          errors : errors
        });
      }
    });
  });
                                      
module.exports = {  
  instance : router
};
