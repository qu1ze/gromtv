var underscore = require('underscore')
  , express = require('express')
  , path = require('path')
  , nconf = require('./../config').nconf
  , PostModel = require('./../models/PostModel')
  , CategoryModel = require('./../models/CategoryModel')
  , suCtrl = require('./../components/Controller.js');

var router = express.Router()
  , env = nconf.get('NODE_ENV')
  , prefix = '/post';

router.route(prefix+'/:id')
  .get(function(req, res) {
    CategoryModel.model().findAll(function(err, categories) {
      PostModel.model().findByPk(req.params.id, function(err, post) {
        post = post.getAttributes();

        post.isYoutube = false;
        if(PostModel.isYoutubeLink(post.mediaUrl)) {
          post.isYoutube = true;
          post.mediaUrl = 'https://www.youtube.com/embed/'+PostModel.getYoutubeVideoId(post.mediaUrl);
        }

        post.content = post.content.replace((new RegExp('{youtube}(.*?){\/youtube}', 'ig')), '<iframe width="560" height="315" src="https://www.youtube.com/embed/$1?rel=0>"></iframe>');

        PostModel.model().findAll(function(err, posts) {
          var resultPosts = [];
          (posts._data ? posts._data.reverse() : []).forEach(function(post){

            if(PostModel.isYoutubeLink(post.mediaUrl)) {
              post.mediaUrl = 'https://i1.ytimg.com/vi/'+PostModel.getYoutubeVideoId(post.mediaUrl)+'/default.jpg';
            }
            post.content = post.content.replace((new RegExp('{youtube}.*?{\/youtube}', 'ig')), '');
            resultPosts.push(post);
          });

          res.render('index', {
            title : post.title+'. Гром TV - Громадське телебачення',
            posts : resultPosts,
            post : post,
            categories : categories._data ? categories._data : []
          });
        });

      });
    });
  });

module.exports = {
  instance : router
};
