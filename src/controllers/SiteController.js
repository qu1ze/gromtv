var underscore = require('underscore')
  , express = require('express')
  , path = require('path')
  , passport = require('passport')
  , nconf = require('./../config').nconf
  , UserModel = require('./../models/UserModel')
  , CategoryModel = require('./../models/CategoryModel')
  , UserContract = require('./../contracts/UserContract')
  , suCtrl = require('./../components/Controller.js');

var router = express.Router()
  , env = nconf.get('NODE_ENV')
  , indexHtml = !env || env == 'development' ? '/index.html' : '/dist/index.html'
  , frontendIndex = path.join(__dirname, '../', '../frontend', indexHtml)
  , backendIndexHtml = !env || env == 'development' ? '/backendIndex.html' : '/dist/backendIndex.html'
  , backendIndex = path.join(__dirname, '../', '../frontend', backendIndexHtml)
  , prefix = '/site';

router.get([
  '/',
  '/user/edit',
  '/profile/:id',
  '/post',
  '/post/create',
  '/post/edit/:id',
  '/category',
  '/category/create',
  '/category/edit/:id',
  '/error'
], function(req, res) {
  if (req.isAuthenticated() && req.user instanceof UserModel) {
    res.render(~req.user.getAttributes('role').indexOf('admin') ? backendIndex : frontendIndex, {
      user : req.user
    });
  } else {
    CategoryModel.model().findAll(function(err, categories) {
      res.render('index', {
        user : req.user,
        categories : categories._data ? categories._data : []
      });
    });
  }
});

router.route(suCtrl.unitePath(prefix, '/signin'))
  .get(function(req, res) {
    var flash = req.flash()
      , options = flash && flash.error ? { errors : flash.error } : { errors : [] };

    res.render('signin', options);
  })
  .post(function(req, res) {
    passport.authenticate('local', {
      successRedirect: '/',
      failureRedirect: '/signin',
      failureFlash: true
    })(req, res);
  });

router.route(suCtrl.unitePath(prefix, '/signup'))
  .get(function(req, res) {
    res.render('signup', {
      errors : null
    });
  })
  .post(function(req, res) {
    UserContract.validate(req, function(errors, data) {
      if (errors) {
        return res.render('signup', {
          errors : UserContract.errorResponse(errors)
        });
      }

      var user = new UserModel;
      user.setAttributes(data);
      user.save(function(err, result) {
        if (err) {
          res.render('signup', {
            errors : UserContract.errorResponse(err)
          });
        } else {
          res.redirect('/');
        }
      });

    });
  });

router.get(suCtrl.unitePath(prefix, '/logout'), function(req, res) {
  req.logout();
  res.redirect('/');
});

module.exports = {
  instance : router
};
